# graphql-doc

Just a brief document describing how awesome is GraphQL and how to use it, especially on Gatsby.


# What is GraphQL?
GraphQL is a query language that lets the client(frontend) select the data it needs to do the request to the server.
This interface, between the client and the server, also makes retrieving data and manipulations more efficient because it only makes requests of data needed, rather than retrieving a fixed set of data as seen in REST (that I will explain later on).

# Differences between GraphQL and Rest
The two main differences between both methods are these:
- GET, POST, DELETE, etc from Rest aren't used in GraphQL.
- Rest gives you a bunch of endpoints, while Graphql is one single endPoint, based on your query.

## example:
Your application is a search tool to look for musicians. After search an artist, you can click on it and see all albums that they have. 

Ok, Let's suppose that you want to get all albums from a specific artist.
in rest APIs:
the endpoint of your request will be like this:
.../artists/
and it will give you something like this:

```
[{
    id:4242,
    groupname: "Musician Y",
    label: Warner,
    bio: 'lorem ispum...',
    lefthand: true,
    ....
},
...
]
```
Just this request bring us a lot of data that we don't need. In this case, we just need the ID,
to make another request to another endpoint to see the albums of this artist.
So, we make a new request, like this:
.../artists/:id/albums/
and it will give you something like this:
```
[{
    id:0,
    albumName: "The best of Y",
    label: Warner,
    Musics: ['x','z','y'],
    ....
},
...
]
```
Ok, so just in this example we called the API a bunch of times and since we just needed the album name, we got a bunch of useless data in the process.

This same process in GraphQL is a lot simpler:
your query would be just what you need for making your app:
```
query{
    artists{
        groupname
        albums{
            albumName
        }
    }
}
```
Yes! Just that.
With one query, you get all data that you want to make all content.

# Is the end of CRUD?
Not necessarily. Although, GraphQL fixes the problem of Over-Fetching that REST requests bring.

Even GraphQL not using CREATE, POST, etc. as methods, it has another 3 methods:
- query: when you want to look for data. you can't change anything on your application with it.
- mutation: It is used when you want to edit some data on your API.
- subscription: for someone that already played with WebSockets, this is kind the same way broadcast works. If you want to listen to something in realtime. I didn't play yet with this one.

and the biggest thing for me is that you can make a Mutation and Query in the same request:
```
{
    //adding a new award to the musician
    mutation updateMusician {
        updateAwards (id:5){
            Award: 'Grammy'
        }
    }

    query Musicians{
        user{
            id
            name
        }
    }

}
```

# The skeleton of a GraphQL query:
```
query Myquery{ #this is the root of a query.
    content{ #what 'session' do you want to do your query
        edges{ #parent element
            node{ #a single item
                bar #content
                foo #content
            }
        }
    }
}
```

You can also sort and filter elements inside of the query!
```
query Myquery{ #this is the root of a query.
    content(sort: {order: DESC, fields:bar} limit:1){  #This will bring only one (the last added item) from the query
        edges{ #parent element
            node{ #a single item
                bar #content
                foo #content
            }
        }
    }
}
```

# How to use in Gatsby:
You can use GraphQL in any component of your website. To request some data over GraphQL, you may need to install some special plugin (Like the MarkdownRemark, a plugin to add MD files, like this one, as an HTML page on your website) 

The query to show a list of posts in a blog is something like this:
```
 const data = useStaticQuery(graphql`
    query{
      allMarkdownRemark{
        edges{
          node{
            frontmatter{
              title
              date
              featuredImage {
                childImageSharp {
                  fluid {
                    src
                  }
                }
              }
            }
            fields{
              slug
            }
            excerpt
          }
        }
      }
    }
    `)
```
If you run a console.log you will see a object with all content ready to use in your website:
```
console.log(data.allMarkdownRemark.edges)
```

## How to use data from a query on my website?
With JSX, you can run a foreach or map in a query and get all posts in your page:
```
        <ol>
            {data.allMarkdownRemark.edges.map((edge) => {
                return(
                  <div key={edge.node.fields.slug} className="Post">
                    <div className="PostThumb">
                    <img src={edge.node.frontmatter.featuredImage.childImageSharp.fluid.src} alt="thumb"/>
                    </div>
                    <div className="TextPreview">
                      <h2>{edge.node.frontmatter.title}</h2>
                      <p>{edge.node.frontmatter.date}</p>
                      <p>{edge.node.excerpt}</p>
                      <Link to={`projects/${edge.node.fields.slug}`}>See More...</Link>
                    </div>
                  </div>
                )
            })}
        </ol> 
```

# fun facts and tips:

Gatsby, by default, brings a tool called GraphiQL. GraphiQL is kiiiind like Insomnia or Postman, to access it just type:
localhost:PORT/___graphql

You don't need to make one query by request. You can add more, like this:
POST /Albums
```
{
    query AllMusicians{
        users()
    }

    query AllAlbums{
        Albums()
    }
}
```
